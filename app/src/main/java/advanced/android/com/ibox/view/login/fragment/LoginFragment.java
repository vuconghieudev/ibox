package advanced.android.com.ibox.view.login.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import advanced.android.com.ibox.Keybroad;
import advanced.android.com.ibox.R;

/**
 * Created by Hieu on 04-Jun-18.
 */

public class LoginFragment extends Fragment {
    EditText edtUsername,edtPassword;
    Keybroad k ;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_login,container, false);
        k = new Keybroad(getActivity());
        addControls(v);
        addEvents(v);
        return v;
    }

    private void addControls(View v) {
        edtUsername = (EditText) v.findViewById(R.id.edtUsername);
        edtPassword = (EditText) v.findViewById(R.id.edtPassword);

    }
    private void addEvents(View v) {

        k.actionDoneHideKeyboard(edtPassword);

    }
}

package advanced.android.com.ibox;

import android.app.Activity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Hieu on 07-Jun-18.
 */

public class Keybroad {
    public  Activity activity;
    public Keybroad(Activity activity) {
        this.activity = activity;
    }
    public  void setupUI(View view) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard();
                    return false;
                }
            });
        }
    }
    public  void actionDoneHideKeyboard(EditText editText){
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i == EditorInfo.IME_ACTION_DONE){
                   activity.findViewById(R.id.edtHide).requestFocus();
                   hideSoftKeyboard();
                    return  true;
                }
                return false;
            }
        });
    }
    public  void hideSoftKeyboard() {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
        EditText edtHide = activity.findViewById(R.id.edtHide);
        edtHide.requestFocus();
    }
}
